FROM amazoncorretto:17 AS builder

WORKDIR /app
ARG SERVICE_NAME

COPY . .
RUN ./gradlew --no-daemon
RUN chmod +x gradlew
RUN ./gradlew clean dependencies -i --no-daemon

RUN GRADLE_OPTS="-XX:MaxRAMPercentage=90.0" ./gradlew bootJar -p $SERVICE_NAME -i --stacktrace && \
    java -Djarmode=layertools -jar $SERVICE_NAME/build/libs/*SNAPSHOT.jar extract --destination /output && \
    rm -rf /app
RUN ls /output


FROM amazoncorretto:17
WORKDIR application
COPY --from=builder /output/dependencies/ ./
COPY --from=builder /output/spring-boot-loader/ ./
COPY --from=builder /output/snapshot-dependencies/ ./
COPY --from=builder /output/application/ ./
EXPOSE 8080

ENV JAVA_OPTS='-XX:MaxRAMPercentage=90.0'
ENTRYPOINT ["java","org.springframework.boot.loader.launch.JarLauncher"]
