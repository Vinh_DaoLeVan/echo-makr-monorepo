import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "ycomm.vn"
version = "0.0.1-SNAPSHOT"

plugins {
    id("org.springframework.boot") version "3.2.5"
    id("io.spring.dependency-management") version "1.1.4"
    kotlin("jvm") version "1.9.23"
    kotlin("plugin.spring") version "1.9.23"
    id("org.jetbrains.kotlin.plugin.jpa") version "1.7.22"


    //KOTLIN LOMBOK https://kotlinlang.org/docs/lombok.html#c851053d
    id("org.jetbrains.kotlin.plugin.lombok") version "1.8.21"
    id("io.freefair.lombok") version "8.0.1"
    id("org.flywaydb.flyway") version "9.8.1"
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
}

repositories {
    mavenCentral()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs += "-Xjsr305=strict"
        jvmTarget = "17"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

subprojects {
    repositories {
        mavenCentral()
    }
    configure(listOf("admin-service", "auth-service", "common-service", "user-service")) {
        apply(plugin = "io.spring.dependency-management")
        apply(plugin = "org.springframework.boot")
        apply(plugin = "org.jetbrains.kotlin.jvm")
        apply(plugin = "org.jetbrains.kotlin.plugin.spring")
        apply(plugin = "org.jetbrains.kotlin.plugin.lombok")
        apply(plugin = "io.freefair.lombok")
        apply(plugin = "org.flywaydb.flyway")
        group = "ycomm.vn"
        version = "0.0.1-SNAPSHOT"

        dependencies {
            implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:2.1.0")
            implementation("sh.ory:ory-client:1.6.2")
            implementation("org.springframework.boot:spring-boot-starter-security")
            implementation("org.springframework.boot:spring-boot-starter")
            testImplementation("org.springframework.boot:spring-boot-starter-test")

            implementation("org.springframework.boot:spring-boot-starter-data-jdbc")
            implementation("org.springframework.boot:spring-boot-starter-data-jpa:3.2.5")
            implementation("com.querydsl:querydsl-jpa:5.0.0")

            // MySQL Connector
            implementation("mysql:mysql-connector-java:8.0.32")

            implementation("org.springframework.boot:spring-boot-starter-data-jdbc")

            implementation("org.springframework.boot:spring-boot-starter-web")
            implementation(platform("org.springframework.boot:spring-boot-starter-parent:3.2.5"))
            implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

            implementation("org.springframework.boot:spring-boot-starter-validation")
            implementation("org.flywaydb:flyway-core")
            implementation("org.flywaydb:flyway-mysql")

            implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
            implementation("org.jetbrains.kotlin:kotlin-reflect")
            implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
            implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.2")
            implementation("io.ktor:ktor-client-core:${"2.3.0"}")
            implementation("io.ktor:ktor-client-cio:${"2.3.0"}")
            implementation("io.ktor:ktor-client-cio-jvm:2.3.0")

            compileOnly("org.projectlombok:lombok")
            annotationProcessor("org.projectlombok:lombok")
            implementation("io.github.microutils:kotlin-logging-jvm:2.0.11")
        }
    }
}



tasks.bootJar {
    enabled = false  // Disable BootJar task in main project
}

