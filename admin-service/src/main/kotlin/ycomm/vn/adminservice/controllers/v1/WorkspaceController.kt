package ycomm.vn.adminservice.controllers.v1

import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import ycomm.vn.adminservice.controllers.BaseController
import ycomm.vn.adminservice.dto.workspace.service.CreateWorkspaceReq
import ycomm.vn.adminservice.dto.workspace.service.WorkspaceInfoRes
import ycomm.vn.adminservice.services.WorkspaceService
import ycomm.vn.adminservice.utils.spring.security.SecurityContextUtils
import ycomm.vn.dto.AppResponse

@RestController
@RequestMapping("/v1/workspace")
class WorkspaceController(
    private val workspaceService: WorkspaceService,
    private val securityContextUtils: SecurityContextUtils
) : BaseController() {

    @PostMapping("")
    @PreAuthorize("hasPermission('', 'WorkspaceEntity', 'USER_CREATE_WORKSPACE')")
    fun createWorkspace(@RequestBody workspaceInfo: CreateWorkspaceReq): AppResponse<WorkspaceInfoRes> {
        return AppResponse.success(workspaceService.create(workspaceInfo))
    }

    @GetMapping("/{workspaceId}")
    @PreAuthorize("hasPermission(#workspaceId, 'WorkspaceEntity', 'USER_VIEW_WORKSPACE')")
    fun getWorkspace(@PathVariable workspaceId: Long): AppResponse<WorkspaceInfoRes> {
        return AppResponse.success(workspaceService.get(workspaceId))
    }

}