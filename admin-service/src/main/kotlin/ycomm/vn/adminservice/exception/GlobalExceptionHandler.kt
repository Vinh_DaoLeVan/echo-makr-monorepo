package ycomm.vn.adminservice.exception

import org.springframework.web.bind.annotation.ControllerAdvice
import ycomm.vn.exception.CommonExceptionHandler

@ControllerAdvice
class GlobalExceptionHandler : CommonExceptionHandler() {
}