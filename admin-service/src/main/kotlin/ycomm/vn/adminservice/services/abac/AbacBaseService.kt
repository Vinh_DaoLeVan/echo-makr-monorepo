package ycomm.vn.adminservice.services.abac

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ycomm.vn.adminservice.entities.WorkspaceEntity
import ycomm.vn.enums.PermissionEnum
import ycomm.vn.interfaces.IAbacService

@Service
class AbacBaseService : IAbacService {

    @Autowired
    lateinit var workSpacePolicy: WorkSpacePolicy

    override fun check(recordId: Long, recordEntityClass: Class<*>, permission: PermissionEnum): Boolean {
        return when (recordEntityClass) {
            WorkspaceEntity::class.java -> {
                workSpacePolicy.check(recordId, permission)
            }
            else -> false
        }

    }

}