package ycomm.vn.adminservice.services

import org.springframework.stereotype.Service
import ycomm.vn.adminservice.dto.workspace.service.CreateWorkspaceReq
import ycomm.vn.adminservice.dto.workspace.service.WorkspaceInfoRes
import ycomm.vn.adminservice.entities.WorkspaceEntity
import ycomm.vn.adminservice.repositories.WorkspaceRepository


@Service
class WorkspaceService(
    private val workspaceRepository: WorkspaceRepository,
) {

    fun create(workspaceInfo: CreateWorkspaceReq): WorkspaceInfoRes {

        val result = workspaceRepository.save(WorkspaceEntity().apply {
            this.name = workspaceInfo.name
        })

        return WorkspaceInfoRes(result.id, result.name)
    }

    fun get(workspaceId: Long): WorkspaceInfoRes {
        val result = workspaceRepository.findById(workspaceId).get()
        return WorkspaceInfoRes(result.id, result.name)
    }

}