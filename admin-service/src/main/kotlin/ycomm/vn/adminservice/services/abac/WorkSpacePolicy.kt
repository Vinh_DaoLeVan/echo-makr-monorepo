package ycomm.vn.adminservice.services.abac

import org.springframework.stereotype.Component
import ycomm.vn.enums.PermissionEnum
import ycomm.vn.adminservice.repositories.WorkspaceRepository
import ycomm.vn.interfaces.IAbacPolicy

@Component
class WorkSpacePolicy(val workspaceRepository: WorkspaceRepository): IAbacPolicy {

    override fun check(recordId: Long, permission: PermissionEnum): Boolean {
        val workspace = workspaceRepository.findById(recordId).orElseThrow { IllegalArgumentException("Workspace not found") }
        return when(permission) {
            PermissionEnum.USER_VIEW_WORKSPACE -> {
                !workspace.disabled
            }
            else -> false
        }
    }

}