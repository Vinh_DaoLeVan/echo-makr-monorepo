package ycomm.vn.adminservice.dto.workspace.service

data class WorkspaceInfoRes(
    val id: Long,
    val name: String
)
