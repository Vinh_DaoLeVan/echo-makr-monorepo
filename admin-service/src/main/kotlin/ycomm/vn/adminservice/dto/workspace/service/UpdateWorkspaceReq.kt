package ycomm.vn.adminservice.dto.workspace.service

class UpdateWorkspaceReq(
    val id: Long,
    val name: String
) {
}