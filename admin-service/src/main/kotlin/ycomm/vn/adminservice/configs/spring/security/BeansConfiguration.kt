package ycomm.vn.adminservice.configs.spring.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ycomm.vn.configs.CommonBeans
import ycomm.vn.services.AuthUserInfoService

@Configuration
class BeansConfiguration : CommonBeans() {

    @Bean
    fun authenticationFilter(authUserInfoService: AuthUserInfoService): AuthenticationFilter {
        return AuthenticationFilter(authUserInfoService)
    }
}