package ycomm.vn.adminservice.configs.gson

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ycomm.vn.configs.GsonConfiguration

@Configuration
class GsonConfiguration : GsonConfiguration() {
    @Bean
    override fun gson(gsonBuilder: GsonBuilder): Gson {
        return super.gson(gsonBuilder)
    }
}