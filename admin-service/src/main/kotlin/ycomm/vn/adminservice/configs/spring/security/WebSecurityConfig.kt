package ycomm.vn.adminservice.configs.spring.security

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.access.AuthorizationServiceException
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configurers.CorsConfigurer
import org.springframework.security.config.annotation.web.configurers.ExceptionHandlingConfigurer
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.servlet.HandlerExceptionResolver
import org.springframework.security.access.AccessDeniedException

@Configuration
@EnableWebSecurity
@EnableMethodSecurity(prePostEnabled = true)
class WebSecurityConfiguration(
    val authenticationFilter: AuthenticationFilter,
) {

    private val logger = KotlinLogging.logger {}


    @Autowired
    @Qualifier("handlerExceptionResolver")
    private lateinit var resolver: HandlerExceptionResolver


    @Bean
    @Throws(Exception::class)
    fun filterChain(http: HttpSecurity): SecurityFilterChain {
        http
            .authorizeHttpRequests { c ->
                c.anyRequest().authenticated()
            }
            .cors { c: CorsConfigurer<HttpSecurity?> ->
                c.configurationSource(corsConfigurationSource())
            }
            .exceptionHandling { c: ExceptionHandlingConfigurer<HttpSecurity> ->
                c.authenticationEntryPoint { request, response, authException ->
                    logger.error { "#WebSecurityConfig.authenticationEntryPoint" }
                    resolver.resolveException(
                        request!!,
                        response!!,
                        null,
                        AccessDeniedException("Access denied")
                    )
                }
                c.accessDeniedHandler { request, response, accessDeniedException ->
                    logger.error { "#WebSecurityConfig.accessDeniedHandler" }
                    resolver.resolveException(
                        request!!,
                        response!!,
                        null,
                        AuthorizationServiceException("Access Denied")
                    )
                }
            }
            .addFilterBefore(this.authenticationFilter, UsernamePasswordAuthenticationFilter::class.java)
            .httpBasic(Customizer.withDefaults())
        return http.build()
    }


    private fun corsConfigurationSource(): CorsConfigurationSource {
        val configuration = CorsConfiguration()
        configuration.allowCredentials = true
        configuration.allowedOriginPatterns = mutableListOf("*")
        configuration.allowedHeaders = mutableListOf("*")
        configuration.allowedMethods = mutableListOf("*")
        configuration.maxAge = 60 * 60L //1 hour
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", configuration)
        return source
    }

}