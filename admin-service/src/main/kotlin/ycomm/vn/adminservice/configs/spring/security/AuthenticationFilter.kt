package ycomm.vn.adminservice.configs.spring.security

import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import ycomm.vn.adminservice.dto.spring.security.SecurityUserDetail
import ycomm.vn.services.AuthUserInfoService

@Component
class AuthenticationFilter(
    val authUserInfoService: AuthUserInfoService
) : OncePerRequestFilter() {

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        try {
//            val cookie = request.getHeader("Cookie")
//            val session = frontendApi.toSession(null, cookie, null) ?: throw AccessDeniedException("Unauthorized")
//
//            if (session.active== false) {
//                throw AccessDeniedException("Your session has expired")
//            }
//
//            identityApi.extendSession(session.id)
            val user = authUserInfoService.checkWhoAmI(request)
            println("AuthenticationFilter.User: $user")
            val authorities = user.roles.map { SimpleGrantedAuthority(it.name) }

            val userDetail = SecurityUserDetail(user.email, authorities, user)
            SecurityContextHolder.getContext().authentication =
                UsernamePasswordAuthenticationToken(userDetail, request.getHeader("Cookie"), userDetail.authorities)
            filterChain.doFilter(request, response)
        } catch (e: Exception) {
            e.printStackTrace()
            SecurityContextHolder.clearContext()
            response.sendError(401, "Unauthorized")
            filterChain.doFilter(request, response)
        }
    }
}