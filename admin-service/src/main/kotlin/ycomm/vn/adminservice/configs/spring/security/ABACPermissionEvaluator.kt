package ycomm.vn.adminservice.configs.spring.security

import io.ktor.util.reflect.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.PermissionEvaluator
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import ycomm.vn.adminservice.dto.spring.security.SecurityUserDetail
import ycomm.vn.enums.BaseRoleEnum
import ycomm.vn.enums.PermissionEnum
import ycomm.vn.adminservice.services.abac.AbacBaseService
import ycomm.vn.services.AuthUserInfoService
import java.io.Serializable

@Component
class ABACPermissionEvaluator(
) : PermissionEvaluator {

    @Autowired
    private lateinit var authUserInfoService: AuthUserInfoService

    @Autowired
    lateinit var abacBaseService: AbacBaseService

    fun validatePermission(permission: Any) {
        if (!PermissionEnum.valueOf(permission as String).instanceOf(PermissionEnum::class)) {
            throw IllegalArgumentException("Invalid permission")
        }
    }

    override fun hasPermission(authentication: Authentication?, targetDomainObject: Any?, permission: Any?): Boolean {
        TODO("Not yet implemented")
    }

    override fun hasPermission(
        auth: Authentication,
        recordId: Serializable,
        recordType: String,
        permission: Any
    ): Boolean {
        this.validatePermission(permission)
        if (!auth.isAuthenticated)
            return false

        return hasPrivilege(
            auth,
            recordId as Long,
            recordType,
            PermissionEnum.valueOf(permission as String)
        )
    }

    private fun hasPrivilege(auth: Authentication, recordId: Long, recordType: String, action: PermissionEnum): Boolean {
        try {
            val recordEntityClassName = "ycomm.vn.adminservice.entities.$recordType"
            val rbacCheck = auth.isAuthenticated && authUserInfoService.checkPermission(auth.credentials as String, recordId, action.name)

            return if (!rbacCheck)
                false
            else {
                abacBaseService.check(
                    recordId,
                    Class.forName(recordEntityClassName),
                    action
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
    }

}