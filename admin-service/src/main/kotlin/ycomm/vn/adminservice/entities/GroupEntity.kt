package ycomm.vn.adminservice.entities

import jakarta.persistence.*
import ycomm.vn.entities.BaseEntity

@Entity
@Table(name="`groups`")
class GroupEntity : BaseEntity() {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    lateinit var name: String

    @Column(name = "workspace_id", insertable = false, updatable = false)
    var workspaceId: Long? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "workspace_id")
    var workspace: WorkspaceEntity? = null

    @OneToMany(mappedBy = "group", cascade = [CascadeType.ALL], orphanRemoval = true)
    var serieses: MutableList<SeriesEntity> = mutableListOf()

}