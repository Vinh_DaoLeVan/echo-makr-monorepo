package ycomm.vn.adminservice.entities

import jakarta.persistence.*
import ycomm.vn.entities.BaseEntity

@Entity
@Table(name="episodes")
class EpisodeEntity : BaseEntity() {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    lateinit var name: String

    @Column(name = "series_id", insertable = false, updatable = false)
    var seriesId: Long? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "series_id")
    lateinit var series: SeriesEntity

}