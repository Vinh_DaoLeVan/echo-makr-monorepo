package ycomm.vn.adminservice.entities

import jakarta.persistence.*
import ycomm.vn.entities.BaseEntity

@Entity
@Table(name = "series")
class SeriesEntity : BaseEntity() {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    lateinit var name: String

    @Column(name = "group_id", insertable = false, updatable = false)
    var groupId: Long? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id")
    lateinit var group: GroupEntity
}