package ycomm.vn.adminservice.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ycomm.vn.adminservice.entities.EpisodeEntity

interface EpisodeRepository : JpaRepository<EpisodeEntity, Long> {
}