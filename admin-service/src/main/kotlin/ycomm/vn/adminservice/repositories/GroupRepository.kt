package ycomm.vn.adminservice.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ycomm.vn.adminservice.entities.GroupEntity

@Repository
interface GroupRepository : JpaRepository<GroupEntity, Long> {
}