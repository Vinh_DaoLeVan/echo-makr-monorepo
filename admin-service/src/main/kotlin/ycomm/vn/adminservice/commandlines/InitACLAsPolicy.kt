package ycomm.vn.adminservice.commandlines

import lombok.AllArgsConstructor
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import ycomm.vn.adminservice.entities.*
import ycomm.vn.enums.BaseRoleEnum
import ycomm.vn.enums.PermissionEnum
import ycomm.vn.adminservice.repositories.*

@Component
@AllArgsConstructor
class InitACLAsPolicy(
    val workspaceRepository: WorkspaceRepository,
    val groupRepository: GroupRepository,
    val seriesRepository: SeriesRepository,
    val episodeRepository: EpisodeRepository,
) : CommandLineRunner {

    override fun run(vararg args: String?) {
        try {
            createTestData()
        } catch (ex: Exception) {
            throw ex
        }
    }

    fun initBasePermissions() {
//        val userTest = userRepository.findFirstByEmail("${BaseRoleEnum.SUPER_SYSTEM_ADMIN.roleName.lowercase()}@ycomm.vn").get()
//        val workspace = workspaceRepository.findById(1).get()
//
//        enforcer.addPermissionForUser(BaseRoleEnum.SUPER_SYSTEM_ADMIN.roleName, WorkspaceEntity::class.java.name, PermissionEnum.USER_CREATE_WORKSPACE.name)
//        enforcer.addPermissionForUser(userTest.name, workspace.id.toString(), PermissionEnum.USER_VIEW_WORKSPACE.name)
//        enforcer.addPermissionForUser(userTest.name, workspace.id.toString(), PermissionEnum.USER_EDIT_WORKSPACE.name)
    }

    fun createTestData() {
        if (workspaceRepository.findAll().isEmpty()) {
            workspaceRepository.save(WorkspaceEntity().apply {
                this.name = "workspace1"
            })
        }

        if (groupRepository.findAll().isEmpty()) {
            groupRepository.save(GroupEntity().apply {
                this.name = "group1"
                this.workspace = WorkspaceEntity().apply { id = 1 }
            })
        }

        if (seriesRepository.findAll().isEmpty()) {
            seriesRepository.save(SeriesEntity().apply {
                this.name = "series1"
                this.group = GroupEntity().apply { id = 1 }
            })
        }

        if (episodeRepository.findAll().isEmpty()) {
            episodeRepository.save(EpisodeEntity().apply {
                this.name = "episode1"
                this.series = SeriesEntity().apply { id = 1 }
            })
        }
    }

}