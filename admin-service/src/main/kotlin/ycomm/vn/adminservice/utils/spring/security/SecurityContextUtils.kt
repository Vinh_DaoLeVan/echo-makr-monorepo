package ycomm.vn.adminservice.utils.spring.security

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken
import org.springframework.stereotype.Component
import ycomm.vn.adminservice.dto.spring.security.SecurityUserDetail

@Component
class SecurityContextUtils {

    val authentication: PreAuthenticatedAuthenticationToken?
        get() {
            return SecurityContextHolder.getContext().authentication as PreAuthenticatedAuthenticationToken
        }

    val userDetail: SecurityUserDetail?
        get() {
            return authentication?.principal as SecurityUserDetail
        }

    val email: String?
        get() {
            return userDetail?.email?.trim()
        }
}