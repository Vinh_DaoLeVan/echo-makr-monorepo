create table workspaces
(
    id bigint unsigned unique auto_increment primary key ,
    name varchar(20),
    created_at datetime(3) default current_timestamp(3),
    updated_at datetime(3) default current_timestamp(3)
);

create table `groups`
(
    id bigint unsigned unique auto_increment primary key ,
    name varchar(20),
    workspace_id bigint unsigned,
    foreign key (workspace_id) references workspaces(id),

    created_at datetime(3) default current_timestamp(3),
    updated_at datetime(3) default current_timestamp(3)
);

create table series
(
    id bigint unsigned unique auto_increment primary key ,
    name varchar(20),
    group_id bigint unsigned,
    foreign key (group_id) references `groups`(id),

    created_at datetime(3) default current_timestamp(3),
    updated_at datetime(3) default current_timestamp(3)
);

create table episodes
(
    id bigint unsigned unique auto_increment,
    name varchar(20),
    series_id bigint unsigned
)