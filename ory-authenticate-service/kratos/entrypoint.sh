#!/bin/sh
apk add --no-cache dcron

# shellcheck disable=SC2039
if [[ "$SERVER_FLAG_NAME" == "DEV" ]]; then
#    echo "* * * * * echo 'Hello DEV' " >> /etc/crontabs/root
    echo "0 12 * * * kratos cleanup sql -c /kratos/config.yml " >> /etc/crontabs/root
else
#    echo "* * * * * echo 'Hello ELSE' " >> /etc/crontabs/root
    echo "0 22 * * * kratos cleanup sql -c /kratos/config.yml " >> /etc/crontabs/root
fi

crond -f -l 8 &
kratos serve --dev --config /kratos/config.yml