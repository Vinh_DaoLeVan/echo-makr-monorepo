# Echo Makr Mono Repo

### Local Dev
#### For Develop mode
#### 1. Start relative services (DB & Auth services)
```sh
docker compose -f ory-auth-compose.yml -f docker-compose.yml up --force-recreate --build nginx-proxy db ory-authenticate-service
```
#### 2. Create an IntelliJ Spring for each service run config with
- Set `Run On` with `docker`
  - target of image `amazoncorretto:17`
  - run option `Check debugger-run-options`
  - (replace [absolute-path-to-env-file] with your absolute path)



### Start All Services
```sh
docker compose -f ory-auth-compose.yml -f docker-compose.yml up --force-recreate --build
```