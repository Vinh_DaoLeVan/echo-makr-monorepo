rootProject.name = "echo-makr-mono"
//include("common-service")
//include("auth-service")
//include("admin-service")
//include("user-service")

val projectDirs = listOf("admin-service", "auth-service", "common-service", "user-service")
projectDirs.forEach() {
    include(it)
}