plugins {
    kotlin("jvm") version "1.9.23"
    kotlin("plugin.spring") version "1.9.23"
}

group = "ycomm.vn"  // Adjust group name
version = "1.0.0-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_17
}

repositories {
    mavenCentral()
}

dependencies {
}


tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType(Jar::class) {
    archiveBaseName = "common-service"
}