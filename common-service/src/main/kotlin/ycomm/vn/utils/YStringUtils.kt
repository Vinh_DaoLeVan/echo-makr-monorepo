
class YStringUtils {
    companion object {
        fun isNullOrEmpty(str: String?): Boolean {
            return str.isNullOrEmpty()
        }
        fun trim(str: String?): String {
            return str?.trim() ?: ""
        }
    }
}