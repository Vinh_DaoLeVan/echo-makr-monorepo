package ycomm.vn.services

import com.google.gson.Gson
import com.google.gson.JsonObject
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.util.*
import jakarta.servlet.http.HttpServletRequest
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Service
import ycomm.vn.dto.CheckPermissionReq
import ycomm.vn.dto.WhoIAmUserInfoRes
import ycomm.vn.dto.WhoIAmUserRoleRes

@Service
class AuthUserInfoService(
    val ktorHttpClient: HttpClient,
    val gson: Gson
) {

    companion object {
        const val authServiceUrl = "http://auth-service:8080"
    }

    fun checkWhoAmI(request: HttpServletRequest): WhoIAmUserInfoRes {
        return this.checkWhoAmI(request.getHeader("Cookie") ?: "Hello from kratos")
    }

    fun checkWhoAmI(cookie: String): WhoIAmUserInfoRes {
        val responseBody = runBlocking {
            ktorHttpClient.get("${authServiceUrl}/v1/user/whoami") {
                header("Cookie", cookie)
            }.body<String>()
        }

        val jsonObject = gson.fromJson(responseBody, JsonObject::class.java)
        val dataObject = jsonObject.getAsJsonObject("data")

        return WhoIAmUserInfoRes(
            dataObject["id"].asLong,
            dataObject["email"].asString,
            dataObject["name"].asString,
            dataObject["roles"].asJsonArray.map {
                WhoIAmUserRoleRes(
                    it.asJsonObject["name"].asString
                )
            })
    }

    fun checkPermission(request: HttpServletRequest, recordId: Long, permission: String): Boolean {
        return this.checkPermission(request.getHeader("Cookie") ?: "Hello from kratos", recordId, permission)
    }

    @OptIn(InternalAPI::class)
    fun checkPermission(cookie: String, recordId: Long, permission: String): Boolean {
        val responseBody = runBlocking {
            ktorHttpClient.post("${authServiceUrl}/v1/user/check-permissions") {
                header("Cookie", cookie)
                body = gson.toJson(CheckPermissionReq(recordId, permission))
            }.body<String>()
        }

        val jsonObject = gson.fromJson(responseBody, JsonObject::class.java)
        return jsonObject.getAsJsonObject("data").asBoolean
    }

}