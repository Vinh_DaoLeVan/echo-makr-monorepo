package ycomm.vn.configs

import com.google.gson.*
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import org.springdoc.core.utils.SpringDocUtils
import org.springframework.boot.autoconfigure.gson.GsonBuilderCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component
import java.awt.Color
import java.awt.image.BufferedImage
import java.time.OffsetDateTime
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.function.Consumer



@Component
class ZonedDateTimeTypeAdapter : TypeAdapter<ZonedDateTime>() {
    override fun write(out: JsonWriter?, value: ZonedDateTime?) {
        out?.value(value?.let { it.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME) })
    }

    override fun read(`in`: JsonReader?): ZonedDateTime? {
        return ZonedDateTime.parse(`in`?.nextString())
    }
}

@Component
class OffsetDateTimeTypeAdapter : TypeAdapter<OffsetDateTime?>() {
    override fun write(out: JsonWriter?, value: OffsetDateTime?) {
        out?.value(value?.let { it.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME) })
    }

    override fun read(`inReader`: JsonReader?): OffsetDateTime? {
        return OffsetDateTime.parse(inReader.toString())
    }
}

@Component
class ColorTypeAdapter : TypeAdapter<Color?>() {
    override fun write(out: JsonWriter?, value: Color?) {
        out?.value(value?.let { color -> String.format("#%02X%02X%02X", color.red, color.green, color.blue) })
    }

    override fun read(`inReader`: JsonReader?): Color? {
        return Color.decode(inReader.toString())
    }
}

@Configuration
class GsonConfiguration{
    @Bean
    fun gsonBuilder(customizers: List<GsonBuilderCustomizer>): GsonBuilder? {
        val builder = GsonBuilder()
        customizers.forEach(Consumer { c: GsonBuilderCustomizer -> c.customize(builder) })

        builder.setFieldNamingStrategy(FieldNamingPolicy.UPPER_CASE_WITH_UNDERSCORES)

        //Call replaceWithClass for springdoc openapi too
        SpringDocUtils.getConfig().replaceWithClass(BufferedImage::class.java, String::class.java)
        SpringDocUtils.getConfig().replaceWithClass(ZonedDateTime::class.java, String::class.java)
        SpringDocUtils.getConfig().replaceWithClass(OffsetDateTime::class.java, String::class.java)
        SpringDocUtils.getConfig().replaceWithClass(Color::class.java, String::class.java)
        SpringDocUtils.getConfig().replaceWithClass(JsonObject::class.java, Map::class.java)
        SpringDocUtils.getConfig().replaceWithClass(JsonArray::class.java, List::class.java)
        SpringDocUtils.getConfig().replaceWithClass(JsonElement::class.java, Object::class.java)
        return builder
    }

    @Bean
    fun gson(gsonBuilder: GsonBuilder): Gson {
        return gsonBuilder.create()
    }
}
