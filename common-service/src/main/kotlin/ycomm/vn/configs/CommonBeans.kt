package ycomm.vn.configs

import com.google.gson.Gson
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ycomm.vn.services.AuthUserInfoService

@Configuration
class CommonBeans {

    @Bean
    fun ktorHttpClient(): HttpClient {
        return HttpClient(CIO) {
            engine {
                requestTimeout = 15000 * 3
            }
            this.defaultRequest {
                this.headers { this["User-Agent"] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.3 Safari/605.1.15" }
            }
        }
    }

    @Bean
    fun authUserInfoService(ktorHttpClient: HttpClient, gson: Gson): AuthUserInfoService {
        return AuthUserInfoService(ktorHttpClient, gson)
    }
}
