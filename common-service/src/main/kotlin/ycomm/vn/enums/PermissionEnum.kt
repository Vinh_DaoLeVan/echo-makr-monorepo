package ycomm.vn.enums

enum class PermissionEnum(
    val description: String,
    val action: String,
    val aliasName: String,
    val isResourcePermission: Boolean = false
) {
    ////// Enum should be SUBJECT_ACTION_OBJECT
    ////// Resource Action

    // WORKSPACE IS SUBJECT
    WORKSPACE_BULK_UPLOAD_EPISODE("Bulk Upload Episode", "WORKSPACE_BULK_UPLOAD_EPISODE", "Bulk Upload Episode"),
    WORKSPACE_BRING_KENZA("Bring Kenza", "WORKSPACE_BRING_KENZA", "Bring Kenza"),
    WORKSPACE_BRING_TMS("Bring TMS", "WORKSPACE_BRING_TMS", "Bring TMS"),
    WORKSPACE_BRING_COMICO("Bring Comico", "WORKSPACE_BRING_COMICO", "Bring Comico"),

    // USER IS SUBJECT
    USER_CREATE_WORKSPACE("Create Workspace", "USER_CREATE_WORKSPACE", "Create Workspace"),
    USER_EDIT_WORKSPACE("Edit Workspace", "USER_EDIT_WORKSPACE", "Edit Workspace", true),
    USER_VIEW_WORKSPACE("View Workspace", "USER_VIEW_WORKSPACE", "View Workspace", true),
    USER_DELETE_WORKSPACE("Delete Workspace", "USER_DELETE_WORKSPACE", "Delete Workspace", true),

    USER_CREATE_GROUP("Create Group", "USER_CREATE_GROUP", "Create Group", true),
    USER_EDIT_GROUP("Edit Group", "USER_EDIT_GROUP", "Edit Group", true),
    USER_VIEW_GROUP("View Group", "USER_VIEW_GROUP", "View Group", true),
    USER_DELETE_GROUP("Delete Group", "USER_DELETE_GROUP", "Delete Group", true),
    USER_ASSIGN_GROUP_MEMBER("Assign Group Member", "USER_ASSIGN_GROUP_MEMBER", "Assign Member", true),
    USER_ASSIGN_GROUP_E_MANAGER("Assign Editor Manager", "USER_ASSIGN_GROUP_E_MANAGER", "Assign Editor Manager", true),
    USER_ASSIGN_GROUP_MANAGER("Assign Designer Manager", "GROUP_ASSIGN_D_MANAGER", "Assign Designer Manager", true),
    USER_UNASSIGN_GROUP_MEMBER("Unassign Member", "GROUP_UNASSIGN_MEMBER", "Unassign Member", true),


    USER_CREATE_ACCOUNT("Create Account", "USER_CREATE_ACCOUNT", "Create Account", true),
    USER_EDIT_ACCOUNT("Edit Account", "USER_EDIT_ACCOUNT", "Edit Account", true),
    USER_VIEW_ACCOUNT("View Account", "USER_VIEW_ACCOUNT", "View Account", true),
    USER_DELETE_ACCOUNT("Delete Account", "USER_DELETE_ACCOUNT", "Delete Account", true),

    USER_CREATE_SERIES("Create Series", "USER_CREATE_SERIES", "Create Series", true),
    USER_EDIT_SERIES("Edit Series", "USER_EDIT_SERIES", "Edit Series", true),
    USER_VIEW_SERIES("View Series", "USER_VIEW_SERIES", "View Series", true),
    USER_DELETE_SERIES("Delete Series", "USER_DELETE_SERIES", "Delete Series", true),
    USER_CLOSE_SERIES("Close Series", "USER_CLOSE_SERIES", "Close Series", true),
    USER_ASSIGN_SERIES_MEMBER("Assign Series Member", "USER_ASSIGN_SERIES_MEMBER", "Assign Member", true),
    USER_ASSIGN_SERIES_LEADER("Assign Series Leader", "USER_ASSIGN_SERIES_LEADER", "Assign Leader", true),
    USER_UNASSIGN_SERIES_MEMBER("Unassign Series Member", "USER_UNASSIGN_SERIES_MEMBER", "Unassign Member", true),
    USER_UNASSIGN_SERIES_LEADER("Unassign Series Leader", "USER_UNASSIGN_SERIES_LEADER", "Unassign Leader", true),

    USER_CREATE_GLOSSARIES("Create Glossaries", "USER_CREATE_GLOSSARIES", "Create Glossaries", true),
    USER_EDIT_GLOSSARIES("Edit Glossaries", "USER_EDIT_GLOSSARIES", "Edit Glossaries", true),
    USER_VIEW_GLOSSARIES("View Glossaries", "USER_VIEW_GLOSSARIES", "View Glossaries", true),
    USER_DELETE_GLOSSARIES("Delete Glossaries", "USER_DELETE_GLOSSARIES", "Delete Glossaries", true),
    USER_IMPORT_EXCEL_GLOSSARIES("Import Excel Glossaries", "USER_IMPORT_EXCEL_GLOSSARIES", "Import Excel Glossaries", true),
    USER_EXPORT_EXCEL_GLOSSARIES("Export Excel Glossaries", "USER_EXPORT_EXCEL_GLOSSARIES", "Export Excel Glossaries", true),

    USER_UPLOAD_IMAGE_EPISODE("Upload Image", "USER_UPLOAD_IMAGE_EPISODE", "Upload Image", true),
    USER_UPLOAD_PSD_EPISODE("Upload PSD", "USER_UPLOAD_PSD_EPISODE", "Upload PSD", true),
    USER_EDIT_EPISODE("Edit Episode", "USER_EDIT_EPISODE", "Edit Episode", true),
    USER_DELETE_EPISODE("Delete Episode", "USER_DELETE_EPISODE", "Delete Episode", true),
}