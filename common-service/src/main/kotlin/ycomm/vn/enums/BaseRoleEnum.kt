package ycomm.vn.enums

enum class BaseRoleEnum(val roleName: String, val value: String, val aliasName: String) {
    EDITOR("editor", "editor", "editor_name"),
    DESIGNER("designer", "designer", "designer_name"),
    TRANSLATOR("translator", "translator", "translator_name"),
    VIEWER("viewer", "viewer", "viewer_name"),
    EDITOR_MANAGER("editor_manager", "editor_manager", "editor_manager_name"),
    DESIGNER_MANAGER("designer_manager", "designer_manager", "designer_manager_name"),
    SERIES_LEADER("series_manager", "series_manager", "series_manager_name"),
    USER_SYSTEM_ADMIN("system_admin", "system_admin", "system_admin_name"),
    SUPER_SYSTEM_ADMIN("super_admin", "super_admin", "super_admin_name"),
    WORKSPACE_OWNER("owner", "owner", "owner_name")
}