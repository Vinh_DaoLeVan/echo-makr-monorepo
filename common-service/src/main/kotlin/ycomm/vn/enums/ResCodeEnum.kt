package ycomm.vn.enums

enum class ResCodeEnum(val code: String, val message: String) {
    SUCCESS("0000", "Success"),
    INTERNAL_ERROR("9999", "Internal Error"),

    UN_AUTHENTICATED("0401", "Unauthenticated"),
    UN_AUTHORIZED("0403", "Unauthorized"),
}