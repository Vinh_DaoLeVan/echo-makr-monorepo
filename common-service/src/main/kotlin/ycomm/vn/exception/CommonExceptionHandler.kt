package ycomm.vn.exception

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.access.AuthorizationServiceException
import org.springframework.web.bind.annotation.ExceptionHandler
import ycomm.vn.dto.AppResponse
import ycomm.vn.enums.ResCodeEnum
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

open class CommonExceptionHandler {

    @ExceptionHandler(Exception::class)
    fun handleAllExceptions(e: Exception): ResponseEntity<Any> {

        val internalError = ResCodeEnum.INTERNAL_ERROR

        val appResponse = AppResponse.error<JvmType.Object>(internalError.code, e.message ?: internalError.message)
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(appResponse)
    }

    @ExceptionHandler(AccessDeniedException::class)
    fun handleAccessDeniedException(e: AccessDeniedException): ResponseEntity<Any> {
        val resEnum = ResCodeEnum.UN_AUTHENTICATED
        val appResponse = AppResponse.error<JvmType.Object>(resEnum.code, resEnum.message)
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(appResponse)
    }

    @ExceptionHandler(AuthorizationServiceException::class)
    fun handleAuthorizationServiceException(e: AuthorizationServiceException): ResponseEntity<Any> {
        val resEnum = ResCodeEnum.UN_AUTHORIZED
        val appResponse = AppResponse.error<JvmType.Object>(resEnum.code, resEnum.message)
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(appResponse)
    }

}