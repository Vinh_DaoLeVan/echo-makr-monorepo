package ycomm.vn.interfaces

import ycomm.vn.enums.PermissionEnum

interface IAbacService {
    fun check(recordId: Long, recordEntityClass: Class<*>, permission: PermissionEnum): Boolean
}