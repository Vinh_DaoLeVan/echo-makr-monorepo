package ycomm.vn.interfaces

import ycomm.vn.enums.PermissionEnum

interface IAbacPolicy {
    fun check(recordId: Long, permission: PermissionEnum): Boolean
}