package ycomm.vn.dto

import ycomm.vn.enums.ResCodeEnum
import ycomm.vn.enums.ResResultEnum

class AppResponse<T>(
    var data: T? = null,
    var code: String?,
    var reason: String?,
    var result: String?,
) {

    companion object {
        fun <T> success(
            data: T,
            resCode: ResCodeEnum = ResCodeEnum.SUCCESS,
            resResult: ResResultEnum = ResResultEnum.SUCCESS
        ): AppResponse<T> {
            return AppResponse(data, resCode.code, "", resResult.toString())
        }

        fun <T> error(
            resCode: String,
            resCodeMessage: String,
            resResult: ResResultEnum = ResResultEnum.FAIL
        ): AppResponse<T> {
            return AppResponse(null, resCode, resCodeMessage, resResult.toString())
        }
    }
}
