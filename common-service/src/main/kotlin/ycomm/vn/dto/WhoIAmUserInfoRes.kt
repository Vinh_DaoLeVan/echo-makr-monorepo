package ycomm.vn.dto

import java.io.Serializable

data class WhoIAmUserInfoRes(
    var id: Long,
    var email: String,
    var name: String,
    val roles: List<WhoIAmUserRoleRes>,
) : Serializable
