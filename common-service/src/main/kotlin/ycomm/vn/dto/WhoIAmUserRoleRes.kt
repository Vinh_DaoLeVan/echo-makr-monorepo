package ycomm.vn.dto

import java.io.Serializable

data class WhoIAmUserRoleRes(
    val name: String
) : Serializable