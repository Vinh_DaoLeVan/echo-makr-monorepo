package ycomm.vn.dto

import java.io.Serializable

data class CheckPermissionReq(
    val recordId: Long,
    val permission: String
) : Serializable