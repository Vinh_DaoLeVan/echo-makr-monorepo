package ycomm.vn.entities

import jakarta.persistence.PrePersist
import jakarta.persistence.PreUpdate
import java.time.ZonedDateTime

open class BaseEntity {

    lateinit var createdAt: ZonedDateTime
    lateinit var updatedAt: ZonedDateTime

    @PrePersist
    fun beforeCreate() {
        val now = ZonedDateTime.now()
        this.createdAt = now
        this.updatedAt = now
    }

    @PreUpdate
    fun beforeUpdate() {
        this.updatedAt = ZonedDateTime.now()
    }


}