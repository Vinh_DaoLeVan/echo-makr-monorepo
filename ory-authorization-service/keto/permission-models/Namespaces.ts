import {Context, Namespace, SubjectSet} from "@ory/keto-namespace-types"


class User implements Namespace {

}

class WorkSpace implements Namespace {
    related: {
        editors: User[]
        designers: User[]
        eManagers: User[]
        dManagers: User[]
        owners: User[]
    }
    permits = {
        view: (ctx: Context): boolean => this.related.eManagers.includes(ctx.subject),
        edit: (ctx: Context): boolean => this.related.editors.includes(ctx.subject)
    }
}

class Group implements Namespace {
    related: {
        owners: WorkSpace[]
        assignees: (SubjectSet<WorkSpace, "eManagers"> | SubjectSet<WorkSpace, "dManagers">)[]
    }

    permits = {
        edit: (ctx: Context): boolean => this.related.assignees.includes(ctx.subject),
        view: (ctx: Context): boolean => this.permits.view(ctx)
    }
}

class Serial implements Namespace {
    related: {
        owners: Group[]
        editors: SubjectSet<WorkSpace, "editors">[]
        designers: SubjectSet<WorkSpace, "designers">[]
        eManagers: SubjectSet<Group, "assignees">[]
        dManagers: SubjectSet<Group, "assignees">[]
    }


    permits = {
        edit: (ctx: Context): boolean => this.related.editors.includes(ctx.subject) && this.related.designers.includes(ctx.subject),
        view: (ctx: Context): boolean => this.related.editors.includes(ctx.subject) &&
            this.related.designers.includes(ctx.subject) &&
            this.related.eManagers.includes(ctx.subject) &&
            this.related.dManagers.includes(ctx.subject)
    }
}

class Episode implements Namespace {
    related: {
        owner: Serial[]
        editors: SubjectSet<Serial, "editors">[]
        designers: SubjectSet<Serial, "designers">[]
    }

    permits = {
        edit: (ctx: Context): boolean => this.related.editors.includes(ctx.subject) && this.related.designers.includes(ctx.subject),
        view: (ctx: Context): boolean => this.permits.edit(ctx)
    }
}