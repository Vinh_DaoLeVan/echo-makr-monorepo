package ycomm.vn.userservice.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class TestController : BaseController() {

    @GetMapping("/test")
    fun test(): String {
        return "Hello World"
    }
}