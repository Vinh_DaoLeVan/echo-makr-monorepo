create table work_spaces
(
    id             bigint unsigned unique not null auto_increment primary key,
    slug           varchar(8) unique     not null comment "Slug/Public ID (Format: w + last 2 digit of year  + 5 digit number)",
    name           varchar(50),
    usage_count    bigint unsigned default 0 comment "Current usage count",
    usage_limit    bigint unsigned default 0 comment "Limit of usage",
    is_usage_limit boolean         default false comment "Is limit usage or not",

    created_at     datetime(3)     default current_timestamp(3) comment "Created at time",
    updated_at     datetime(3)     default current_timestamp(3) comment "Updated at time"
);

create table work_space_permissions
(
    id                 bigint unsigned unique not null auto_increment primary key,
    work_space_id      bigint unique          not null comment "Ref workspace id (unique for sure 1-1)",
    can_bring_kiledel  boolean default false  not null comment "Bring Kiledel permission",
    can_bring_comico   boolean default false  not null comment "Bring Comico permission",
    can_bring_kenza    boolean default false  not null comment "Bring Kenza permission",
    can_orc            boolean default false  not null comment "OCR permission",
    can_auto_translate boolean default false  not null comment "Auto Translate permission",
    can_export_psd     boolean default false  not null comment "Export PSD permission",
    can_export_image   boolean default false  not null comment "Export Image permission",
    can_export_excel   boolean default false  not null comment "Export Excel permission",

    created_at     datetime(3)     default current_timestamp(3) comment "Created at time",
    updated_at     datetime(3)     default current_timestamp(3) comment "Updated at time",
    foreign key (work_space_id) references work_spaces (id)
);

create table work_space_default_value_settings
(
    id bigint unsigned unique not null auto_increment primary key,
    work_space_id      bigint unique          not null comment "Ref workspace id (unique for sure 1-1)",


    created_at     datetime(3)     default current_timestamp(3) comment "Created at time",
    updated_at     datetime(3)     default current_timestamp(3) comment "Updated at time",
    foreign key (work_space_id) references work_spaces (id)
)