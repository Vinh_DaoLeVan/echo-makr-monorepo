create table roles
(
    id         bigint unsigned unique auto_increment primary key,
    name       varchar(50) unique,
    value      varchar(50) unique,

    created_at datetime(3) default current_timestamp(3),
    updated_at datetime(3) default current_timestamp(3)
);

create table users
(
    id                 bigint unsigned unique auto_increment primary key,
    kratos_identity_id varchar(10) unique,
    name               varchar(30),
    email              varchar(30) unique,

    created_at         datetime(3) default current_timestamp(3),
    updated_at         datetime(3) default current_timestamp(3)
);

create table roles_permissions
(
    id         bigint unsigned unique auto_increment primary key,
    role_id    bigint unsigned,
    permission varchar(30),

    created_at datetime(3) default current_timestamp(3),
    updated_at datetime(3) default current_timestamp(3),

    foreign key (role_id) references roles (id),
    unique (role_id, permission)
);

create table users_roles
(
    id         bigint unsigned unique auto_increment primary key,
    user_id    bigint unsigned not null,
    role_id    bigint unsigned not null,

    created_at datetime(3) default current_timestamp(3),
    updated_at datetime(3) default current_timestamp(3),

    foreign key (user_id) references users (id),
    foreign key (role_id) references roles (id),
    unique (user_id, role_id)
);

create table information_assigns
(
    id          bigint auto_increment primary key,
    assignee_id bigint unsigned not null comment "User id of account who is assigned",
    role_id     bigint unsigned not null comment "Role id of assigned role",

    assigner_id bigint          not null comment "User id of account who assign permissions (assign by who)",

    record_id   bigint          not null comment "Record id of information (ex: workspace id, group id, series id, episode id, ...)",
    record_type varchar(60)     not null comment "Record type of information could be class or table name (ex: workspace class, group class, series class, episode class, ...)",
    assign_at   datetime(3)     not null comment "Assign at time",

    create_at   datetime(3)     not null,
    update_at   datetime(3)     not null,

    unique (assignee_id, role_id, record_id, record_type),
    foreign key (assignee_id, role_id) references users_roles (user_id, role_id)
);