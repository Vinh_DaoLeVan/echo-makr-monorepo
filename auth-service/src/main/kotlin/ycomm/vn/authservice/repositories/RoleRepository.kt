package ycomm.vn.authservice.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ycomm.vn.authservice.entities.RoleEntity
import java.util.Optional

@Repository
interface RoleRepository : JpaRepository<RoleEntity, Long> {

    fun findFirstByName(name: String): Optional<RoleEntity>

}