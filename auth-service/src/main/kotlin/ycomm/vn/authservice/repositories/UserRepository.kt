package ycomm.vn.authservice.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ycomm.vn.authservice.entities.UserEntity
import java.util.Optional

@Repository
interface UserRepository : JpaRepository<UserEntity, Long>  {
    fun findFirstByName(name: String): Optional<UserEntity>
    fun findFirstByEmail(name: String): Optional<UserEntity>
}