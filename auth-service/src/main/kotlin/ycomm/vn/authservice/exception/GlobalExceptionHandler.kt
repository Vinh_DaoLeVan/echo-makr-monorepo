package ycomm.vn.authservice.exception

import org.springframework.web.bind.annotation.ControllerAdvice
import ycomm.vn.exception.CommonExceptionHandler

@ControllerAdvice
class GlobalExceptionHandler : CommonExceptionHandler() {
}