package ycomm.vn.authservice.services

import org.casbin.jcasbin.main.Enforcer
import org.casbin.jcasbin.persist.file_adapter.FilteredAdapter.Filter
import org.springframework.security.access.AccessDeniedException
import org.springframework.stereotype.Service
import ycomm.vn.authservice.entities.UserEntity
import ycomm.vn.enums.BaseRoleEnum
import ycomm.vn.authservice.repositories.UserRepository
import ycomm.vn.dto.CheckPermissionReq
import ycomm.vn.dto.WhoIAmUserInfoRes
import ycomm.vn.dto.WhoIAmUserRoleRes


@Service
class UserService(
    private val userRepository: UserRepository,
    private val enforcer: Enforcer
) {

    fun fakeUser(): UserEntity {
        return userRepository.findFirstByEmail("${BaseRoleEnum.SUPER_SYSTEM_ADMIN.roleName.lowercase()}@ycomm.vn")
            .orElseThrow {
                throw AccessDeniedException("User not found")
            }
    }

    fun checkWhoAmI(): WhoIAmUserInfoRes {

        val user = fakeUser()

        val filter = Filter()
        filter.g = arrayOf(user.name)
        // TODO check if this is only for role?
        enforcer.loadFilteredPolicy(filter)

        val roleNames = enforcer.getImplicitRolesForUser(user.name).toList()
        val whoIamUserRoleRes: MutableList<WhoIAmUserRoleRes> = mutableListOf()
        for (role in roleNames) {
            whoIamUserRoleRes.add(
                WhoIAmUserRoleRes(role)
            )
        }
        return WhoIAmUserInfoRes(user.id!!, user.email, user.name, whoIamUserRoleRes)
    }

    fun checkPermissions(checkPermissionReq: CheckPermissionReq): Boolean {

        val user = fakeUser()

        val filter = Filter()
        filter.g = arrayOf(user.name)
        filter.p = arrayOf(checkPermissionReq.permission)

        enforcer.loadFilteredPolicy(filter)
        return enforcer.enforce(user.name, checkPermissionReq.recordId.toString(), checkPermissionReq.permission)
    }

}