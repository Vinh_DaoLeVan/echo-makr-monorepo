package ycomm.vn.authservice.commandlines

import lombok.AllArgsConstructor
import org.casbin.jcasbin.main.Enforcer
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import ycomm.vn.authservice.entities.*
import ycomm.vn.enums.BaseRoleEnum
import ycomm.vn.authservice.repositories.*
import ycomm.vn.enums.PermissionEnum

@Component
@AllArgsConstructor
class InitUserTest(
    val enforcer: Enforcer,
    val userRepository: UserRepository,
    val roleRepository: RoleRepository,
) : CommandLineRunner {

    override fun run(vararg args: String?) {
        try {
            setupTestDataSet()

            enforcer.addPermissionForUser("User - ${BaseRoleEnum.USER_SYSTEM_ADMIN.roleName}", "WorkspaceEntity", PermissionEnum.USER_CREATE_WORKSPACE.name)
            enforcer.addPermissionForUser("User - ${BaseRoleEnum.USER_SYSTEM_ADMIN.roleName}", "1", PermissionEnum.USER_EDIT_GROUP.name)
            enforcer.addPermissionForUser("User - ${BaseRoleEnum.USER_SYSTEM_ADMIN.roleName}", "1", PermissionEnum.USER_VIEW_WORKSPACE.name)
        } catch (ex: Exception) {
            throw ex
        }
    }

    fun setupTestDataSet() {
        createRoles()
        createUsers()
    }

    fun createRoles() {
        BaseRoleEnum.entries.forEach {
            roleRepository.findFirstByName(it.roleName).orElseGet {
                roleRepository.save(RoleEntity().apply {
                    this.name = it.roleName
                    this.value = it.value
                })
            }
        }
    }

    fun createUsers() {
        BaseRoleEnum.entries.forEach {
            if (userRepository.findFirstByName("User - ${it.roleName}").isEmpty) {
                val savedUser = userRepository.save(UserEntity().apply {
                    this.name = "User - ${it.roleName}"
                    this.email = "${it.roleName.lowercase()}@ycomm.vn"

                })
                enforcer.addRoleForUser(savedUser.name, it.roleName)
            }
        }
    }
}