package ycomm.vn.authservice.dto.spring.security

import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import ycomm.vn.authservice.entities.UserEntity

data class SecurityUserDetail(val email: String?, val _authorities: Collection<SimpleGrantedAuthority>, val userInfo: UserEntity) :
    User(email, "", _authorities) {
    override fun getAuthorities(): Collection<SimpleGrantedAuthority> {
        return _authorities
    }
}