package ycomm.vn.authservice.controllers

import org.springframework.web.bind.annotation.RestController

@RestController
class BaseController {

    fun sum(a: Int, b: Int): Int {
        return a + b
    }

}