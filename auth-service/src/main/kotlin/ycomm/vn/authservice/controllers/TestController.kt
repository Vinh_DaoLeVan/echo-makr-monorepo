package ycomm.vn.authservice.controllers
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class TestController() : BaseController() {

    @GetMapping("/hello")
    fun hello(): String {
        return ""
    }

}