package ycomm.vn.authservice.controllers.v1

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ycomm.vn.authservice.services.UserService
import ycomm.vn.dto.AppResponse
import ycomm.vn.dto.CheckPermissionReq
import ycomm.vn.dto.WhoIAmUserInfoRes

@RestController
@RequestMapping("/v1/user")
class UserController(
    val userService: UserService
) {

    @GetMapping("whoami")
    fun checkWhoAmI(): AppResponse<WhoIAmUserInfoRes> {
        println("AuthService.checkWhoAmI")
        return AppResponse.success(userService.checkWhoAmI())
    }

    @PostMapping("check-permissions")
    fun checkPermissions(@RequestBody checkPermissionReq: CheckPermissionReq): AppResponse<Boolean> {
        println("AuthService.checkPermissions")
        return AppResponse.success(userService.checkPermissions(checkPermissionReq))
    }

}