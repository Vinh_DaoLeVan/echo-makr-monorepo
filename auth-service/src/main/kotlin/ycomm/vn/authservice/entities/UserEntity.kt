package ycomm.vn.authservice.entities

import jakarta.persistence.*
import ycomm.vn.entities.BaseEntity

@Entity
@Table(name="users")
class UserEntity : BaseEntity() {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    lateinit var name: String
    lateinit var email: String
    lateinit var kratosIdentityId: String

}