package ycomm.vn.authservice.configs.spring.security

import mu.KotlinLogging
import org.casbin.jcasbin.main.Enforcer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.access.AuthorizationServiceException
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configurers.CorsConfigurer
import org.springframework.security.config.annotation.web.configurers.ExceptionHandlingConfigurer
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.servlet.HandlerExceptionResolver
import sh.ory.api.FrontendApi
import sh.ory.api.IdentityApi
import ycomm.vn.authservice.repositories.UserRepository
import org.springframework.security.access.AccessDeniedException

@Configuration
@EnableWebSecurity
@EnableMethodSecurity(prePostEnabled = true)
class WebSecurityConfiguration(
    private val frontendApi: FrontendApi,
    private val identityApi: IdentityApi,
    private val userRepository: UserRepository,
    private val enforcer: Enforcer
) {

    private val logger = KotlinLogging.logger {}


    @Autowired
    @Qualifier("handlerExceptionResolver")
    private lateinit var resolver: HandlerExceptionResolver

    @Bean
    @Throws(Exception::class)
    fun filterChain(http: HttpSecurity): SecurityFilterChain {
        http
            .authorizeHttpRequests { c ->
                c.requestMatchers(
                    "/v1/user/whoami",
                    "/v1/user/check-permissions"
                ).permitAll()
                c.anyRequest().authenticated()
            }
            .cors { c: CorsConfigurer<HttpSecurity?> ->
                c.configurationSource(corsConfigurationSource())
            }
            .exceptionHandling { c: ExceptionHandlingConfigurer<HttpSecurity> ->
                c.authenticationEntryPoint { request, response, authException ->
                    logger.error { "#WebSecurityConfig.authenticationEntryPoint" }
                    authException.printStackTrace()
                    resolver.resolveException(
                        request!!,
                        response!!,
                        null,
                        AccessDeniedException("Access denied")
                    )
                }
                c.accessDeniedHandler { request, response, accessDeniedException ->
                    logger.error { "#WebSecurityConfig.accessDeniedHandler" }
                    accessDeniedException.printStackTrace()
                    resolver.resolveException(
                        request!!,
                        response!!,
                        null,
                        AuthorizationServiceException("Access Denied")
                    )
                }
            }
            .addFilterBefore(
                KratosAuthenticationFilter(frontendApi, identityApi, userRepository, enforcer),
                UsernamePasswordAuthenticationFilter::class.java
            )
            .httpBasic(Customizer.withDefaults())
        return http.build()
    }


    private fun corsConfigurationSource(): CorsConfigurationSource {
        val configuration = CorsConfiguration()
        configuration.allowCredentials = true
        configuration.allowedOriginPatterns = mutableListOf("*")
        configuration.allowedHeaders = mutableListOf("*")
        configuration.allowedMethods = mutableListOf("*")
        configuration.maxAge = 60 * 60L //1 hour
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", configuration)
        return source
    }

}