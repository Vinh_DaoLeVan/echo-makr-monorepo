package ycomm.vn.authservice.configs.ory

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import sh.ory.api.FrontendApi
import sh.ory.api.IdentityApi

@Configuration
class KratosConfig {

    @Bean
    fun identityApi(): IdentityApi {
        val defaultClient = sh.ory.Configuration.getDefaultApiClient()
        val identityApi = IdentityApi(defaultClient)
        identityApi.customBaseUrl = "http://localhost:4434"
        return identityApi
    }

    @Bean
    fun frontendApi(): FrontendApi {
        val defaultClient = sh.ory.Configuration.getDefaultApiClient()
        val frontendApi = FrontendApi(defaultClient)
        frontendApi.customBaseUrl = "http://localhost:4433"
        return frontendApi
    }

}