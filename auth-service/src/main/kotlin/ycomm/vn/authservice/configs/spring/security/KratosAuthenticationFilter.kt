package ycomm.vn.authservice.configs.spring.security

import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.casbin.jcasbin.main.Enforcer
import org.casbin.jcasbin.persist.file_adapter.FilteredAdapter.Filter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import sh.ory.api.FrontendApi
import sh.ory.api.IdentityApi
import ycomm.vn.authservice.dto.spring.security.SecurityUserDetail
import ycomm.vn.enums.BaseRoleEnum
import ycomm.vn.authservice.repositories.UserRepository

@Component
class KratosAuthenticationFilter(
    val frontendApi: FrontendApi,
    val identityApi: IdentityApi,
    val userRepository: UserRepository,
    val enforcer: Enforcer
) : OncePerRequestFilter() {

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        try {
//            val cookie = request.getHeader("Cookie")
//            val session = frontendApi.toSession(null, cookie, null) ?: throw AccessDeniedException("Unauthorized")
//
//            if (session.active== false) {
//                throw AccessDeniedException("Your session has expired")
//            }
//
//            identityApi.extendSession(session.id)

            println("Cookie: ${request.getHeader("Cookie")}")

            val user = userRepository.findFirstByEmail("${BaseRoleEnum.SUPER_SYSTEM_ADMIN.roleName.lowercase()}@ycomm.vn").orElseThrow {
                throw AccessDeniedException("User not found")
            }

            val authorities = enforcer.getImplicitRolesForUser(user.name).map { SimpleGrantedAuthority(it) }.toList()
            val userDetail = SecurityUserDetail(user.email, authorities, user)
            SecurityContextHolder.getContext().authentication =
                UsernamePasswordAuthenticationToken(userDetail, "", userDetail.authorities)
            filterChain.doFilter(request, response)
        } catch (e: Exception) {
            SecurityContextHolder.clearContext()
            response.sendError(401, "Unauthorized")
            filterChain.doFilter(request, response)
        }
    }
}