import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
}

group = "ycomm.vn"
version = "0.0.1-SNAPSHOT"

java {
	sourceCompatibility = JavaVersion.VERSION_17
}

repositories {
	mavenCentral()
}

dependencies {
	implementation(project(":common-service"))
	implementation("org.casbin:casbin-spring-boot-starter:1.3.0")

	implementation("org.casbin:jcasbin:1.54.0")
	implementation("org.casbin:jdbc-adapter:2.6.0")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs += "-Xjsr305=strict"
		jvmTarget = "17"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
